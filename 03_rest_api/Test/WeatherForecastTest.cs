using System;
using Utils;
using Xunit;

namespace Test
{
    public class WeatherForecastTest
    {
        [Fact]
        public void WeatherForecastCtoFTest()
        {
            var forecast = new WeatherForecast {TemperatureC = 30};
            Assert.Equal(85, forecast.TemperatureF);
        }

        [Fact]
        public void DayTest()
        {
            var forecast = new WeatherForecast {Date = DateTime.Today};
            Assert.Equal(DateTime.Today, forecast.Date);
        }
    }
}
