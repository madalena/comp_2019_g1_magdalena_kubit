using System;
using System.Linq;
using CoreAPI.Controllers;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace Test
{
    public class WeatherForecastControllerTest
    {
        [Fact]
        public void GetTestLengthOfArray()
        {
            var loggerMock = new Mock<ILogger<WeatherForecastController>>();
            var controller = new WeatherForecastController(loggerMock.Object);

            var response = controller.Get();
            var arr = response.ToArray();
            Assert.Equal(5, arr.Length);
        }

        [Fact]
        public void GetTestData()
        {
            var loggerMock = new Mock<ILogger<WeatherForecastController>>();
            var controller = new WeatherForecastController(loggerMock.Object);

            var response = controller.Get();
            var arr = response.ToArray();

            Assert.InRange(arr[0].TemperatureC, -20, 55);
        }
    }
}