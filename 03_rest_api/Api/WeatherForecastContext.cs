using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Utils;

namespace CoreAPI
{
    public class WeatherForecastContext : DbContext
    {
        public DbSet<WeatherForecast>?Forecasts { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var sqlitePath = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), 
                @"02_entity_framework_core\weatherForecast.db");

            optionsBuilder.UseSqlite("Data Source=" + sqlitePath + "\\weatherForecast.db");
        }
    }
}