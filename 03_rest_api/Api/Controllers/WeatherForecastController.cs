﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Utils;

namespace CoreAPI.Controllers
{
#pragma warning disable CA1822

    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [HttpPost]
        public IActionResult Post()
        {
            using var db = new WeatherForecastContext();
            /*var project = new Project();
            project.Id = 2;
            project.Name = "Duży projekt";
            project.CreationDate = DateTime.Today;
            project.Description = "Opis dużego projektu";
            project.Uri = new Uri("http://www.google.com");
            project.musiBycNowePole = "ZMIANA";
            db.Projects.Add(project);
            db.SaveChanges();*/
            var forecast = new WeatherForecast();
            forecast.Id = 1;
            forecast.Date = DateTime.Now;
            forecast.TemperatureC = 30;

            db.Forecasts?.Add(forecast);
            db.SaveChanges();
            
            return StatusCode(200);
        }
    }
}
