﻿using System;
using System.IO;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MVC.Models;

namespace MVC.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<Book>?Books { get; set; }
        
        /*protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var sqlitePath = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), 
                @"02_entity_framework_core\books.db");

            optionsBuilder.UseSqlite("Data Source=" + sqlitePath + "\\weatherForecast.db");
        }*/
        
        public ApplicationDbContext(DbContextOptions options)
            : base(options)
        {
        }
    }
}