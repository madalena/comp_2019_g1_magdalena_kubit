using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MVC.Controllers;
using MVC.Models;
using Xunit;

namespace Test
{
    public class TestHomeController
    {
        [Fact]
        public void TestIndex()
        {
            using var homeController = new HomeController();
            var result = homeController.Index();
            
            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public void TestPrivacy()
        {
            using var homeController = new HomeController();
            var result = homeController.Privacy();

            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public void TestError()
        {
            using var homeController = new HomeController
            {
                ControllerContext = new ControllerContext()
                {
                    HttpContext = new DefaultHttpContext() {TraceIdentifier = "1"}
                }
            };

            var result =homeController.Error();
            var vr = Assert.IsType<ViewResult>(result);
            
            var model = vr.ViewData.Model;
            var vm = Assert.IsType<ErrorViewModel>(model);
            
            Assert.Equal("1", vm.RequestId);
        }

        [Fact]
        public void TestErrorViewModel()
        {
            var error = new ErrorViewModel {RequestId = "1"};
            Assert.True(error.ShowRequestId);
        }
    }
}