FROM ubuntu:19.04
RUN apt-get update
RUN apt-get install -y wget
RUN wget https://dot.net/v1/dotnet-install.sh
RUN chmod +x dotnet-install.sh
RUN ./dotnet-install.sh --version 3.0.100
RUN apt-get install -y libicu63
RUN apt-get install -y sqlite3
RUN wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
ENV NVM_DIR /root/.nvm
ENV NODE_VERSION v10.16.0
RUN /bin/bash -c "source $NVM_DIR/nvm.sh && nvm install $NODE_VERSION && nvm use --delete-prefix $NODE_VERSION"
ENV NODE_PATH="$NVM_DIR/versions/node/$NODE_VERSION/lib/node_modules"
ENV PATH="$NVM_DIR/versions/node/$NODE_VERSION/bin:/root/.dotnet:$PATH"
WORKDIR /root
