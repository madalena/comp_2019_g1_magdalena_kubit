﻿using System;
using Microsoft.Extensions.Logging;

namespace Utils
{
    public class Demo
    {
        //private readonly string _name;
        private readonly ILogger<Demo> _logger;

        public Demo( ILogger<Demo> logger)
        {
            _logger = logger;
            //_name = name;
        }

        public void Run(string name)
        {
            _logger.LogInformation($"Hello {name}!");
            //Console.WriteLine($"Hello {_name}!");
        }
    }
}