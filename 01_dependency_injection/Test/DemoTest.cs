using System;
using System.IO;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Moq;
using Utils;
using Xunit;
using ILoggerFactory = Castle.Core.Logging.ILoggerFactory;

namespace Test
{
    public class DemoTest
    {
        [Fact]
        /*public void Run()
        {
            var mock = new Mock<TextWriter>();
            Console.SetOut(mock.Object);

            const string name = "John";
            mock.Setup(tw => tw.WriteLine($"Hello {name}!"));
            
            var demo = 
            demo.Run(name);
        }*/

        public void TestDemoClass()
        {
            /*var mock = new Mock<ILogger<Demo>>();
            ILogger<Demo> logger = mock.Object;
            
            const string name = "John";
            using var serviceProvider = new ServiceCollection()
                .AddLogging(loggingBuilder => loggingBuilder
                    .AddConsole()
                    .SetMinimumLevel(LogLevel.Debug))
                .AddTransient<Demo>()
                .BuildServiceProvider();*/
            var mockLogger = new Mock<ILogger<Demo>>();
            //var mockCatalogueService = new Mock<ICatalogueService>();
            var demo = new Demo(mockLogger.Object);
            demo.Run("John");
            
            //mockLogger.Verify(x => x.Log(LogLevel.Debug, It.IsAny<string>(), It.IsAny<Exception>(), It.IsAny<Func<object, Exception, string>>()), Times.Once);
            //mockLogger.Expect(logger => )
            mockLogger.Verify(logger => logger.Log(LogLevel.Information, It.IsAny<string>, Times.Once()));



        }
    }
}


/*
Co trzeba zrobić?
1. dep inj -> klasa kontener zarządzający zależnościami w aplikacji
2. dodajemy mapowanie 
3. używając tego kontenera można wstrzykiwać zależności
4. w testach trzeba dopisać trochę testów dla kontenera
mamy napisane parę prostych typów

*/