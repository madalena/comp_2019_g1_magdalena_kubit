using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestPlatform.TestHost;
using Xunit;
using ILoggerFactory = Castle.Core.Logging.ILoggerFactory;

namespace Test
{
    internal interface IFoo
    {
        int Test();
    }

    internal class Foo : IFoo
    {
        public int Test()
        {
            return 7;
        }
    }

    internal class Bar
    {
        private readonly IFoo _foo;

        public Bar(IFoo foo) //wstrzykiwanie klasy przez konstruktor
        {
            _foo = foo;
        }

        public int Test()
        {
            return _foo.Test();
        }
    }

    public class DependencyInjectionTest // czy używając kontenera z biblioteki trzeba przetestowac, czy da sie automatycznie wstzykiwać przez konstruktor
    {
        [Fact]
        public void HasSomeTypesForTesting()
        {
            var foo = new Foo();
            var bar = new Bar(foo);
            Assert.Equal(7, bar.Test());
        }


        [Fact]
        public void CreateBarObject()
        {
            var serviceProvider = new ServiceCollection()
                .AddTransient<IFoo, Foo>()
                .AddSingleton<Bar>()
                .BuildServiceProvider();
            
            var bar = serviceProvider.GetService<Bar>();
            Assert.Equal(7, bar.Test());
            Assert.NotNull(bar);
        }

        [Fact]
        public void AddTransientTest()
        {
            var serviceProvider = new ServiceCollection()
                .AddTransient<IFoo, Foo>()
                .BuildServiceProvider();

            var foo1 = serviceProvider.GetService<IFoo>();
            var foo2 = serviceProvider.GetService<IFoo>();
            
            Assert.NotSame(foo1,foo2);
        }

        [Fact]
        public void AddSingletonTest()
        {
            var serviceProvider = new ServiceCollection()
                .AddSingleton<IFoo, Foo>()
                .AddSingleton<Bar>()
                .BuildServiceProvider();

            var bar1 = serviceProvider.GetService<Bar>();
            var bar2 = serviceProvider.GetService<Bar>();

            var foo1 = serviceProvider.GetService<IFoo>();
            var foo2 = serviceProvider.GetService<IFoo>();
            
            Assert.Same(bar1, bar2);
            Assert.Same(foo1, foo2);
        }

        [Fact]
        public void AddScopedTest()
        {
            var serviceProvider = new ServiceCollection()
                .AddScoped<IFoo, Foo>()
                .AddScoped<Bar>()
                .BuildServiceProvider();
            
            var serv1 = serviceProvider.CreateScope();
            var foo1 = serv1.ServiceProvider.GetService<Bar>();
            var foo2 = serv1.ServiceProvider.GetService<Bar>();
            serv1.Dispose();
            
            Assert.Same(foo1, foo2);

            var serv2 = serviceProvider.CreateScope();
            var foo3 = serv2.ServiceProvider.GetService<Bar>();
            serv2.Dispose();
            
            Assert.NotSame(foo1, foo3);
        }
    }

}

// mamy 
//scoped - tworzy się instancja, ale w jakimś zakresie obiektów
// dopisujemy najpierw test dla obiektu typu Bar
// potem sprawdzić, np używając klasy Foo, czy działa dodawania obiektu typu transient, potem dla singletona - zwraca ten sam obiekt
// scoped - w różnych scopach te testy są już różne