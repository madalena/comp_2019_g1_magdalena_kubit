﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Utils;

namespace App
{
    internal class Program
    {
        [ExcludeFromCodeCoverage]
        private static void Main()
        {
            using var serviceProvider = new ServiceCollection()
                .AddLogging(loggingBuilder => loggingBuilder
                    .AddConsole()
                    .SetMinimumLevel(LogLevel.Debug))
                .AddTransient<Demo>()
                .BuildServiceProvider();
            
            var bar = serviceProvider.GetService<Demo>();
            bar.Run("John");
        }
    }
}
