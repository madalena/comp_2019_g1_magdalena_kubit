﻿using Microsoft.EntityFrameworkCore.Migrations;
#pragma warning disable CA1062

namespace App.Migrations
{
    public partial class Initial2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "musiBycNowePole",
                table: "Projects",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "musiBycNowePole",
                table: "Projects");
        }
    }
}
