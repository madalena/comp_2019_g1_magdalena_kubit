﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using Utils;
#pragma warning disable CS8602

namespace App
{
    [ExcludeFromCodeCoverage]
    internal static class Program
    {
        private static void Main()
        {
            using var db = new ProjectContext();
                /*var project = new Project();
                project.Id = 2;
                project.Name = "Duży projekt";
                project.CreationDate = DateTime.Today;
                project.Description = "Opis dużego projektu";
                project.Uri = new Uri("http://www.google.com");
                project.musiBycNowePole = "ZMIANA";
                db.Projects.Add(project);
                db.SaveChanges();*/

                var projectsWithId = db.Projects
                    .Where(b => b.Id == 2)
                    .ToArray();
                
                Console.WriteLine("Project name is: " + projectsWithId[0].Name);
                Console.WriteLine("And Uri is: " + projectsWithId[0].Uri);
                Console.WriteLine("And next field: " + projectsWithId[0].musiBycNowePole);
        }
    }
}

/*
 * 1. metody asynchroniczne
 * 2. refleksje
 * - nie mamy bezpośredniego zapisywania obiektu do bazy ()
 * - robimy te operacje przez context
 * - var post =
 * var context = (przez ctx zapisujemy posta) post to jest zwykła klasa zapisana w c#
 * sam ctx pozwala nam
 * 1) cwiczenie
 * klasa projektu (tej klasy używamy w ctx bazy danych), potem na podstawie tego ctx możemy wygenerować bazę danych
 * na podstawie post i ctx przy użyciu dotnetef wykonujemy migragę (zawiera zestaw metod, umożliwiających strorzenie bazy danych)
 * potem biorąc migacę i znowu używając dotnetef jesteśmy w stanie stworzyć tabelę
 * nie musimy wgl patrzeć na migrację, jak to się dzieje
 * na podstawie post + ctx jesteśmy w stanie zapisywać do bazy danych
 *
 * 
*/