using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Utils;

namespace App
{
    public class ProjectContext : DbContext
    {
        public DbSet<Project>?Projects { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var sqlitePath = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), 
                @"02_entity_framework_core\project.db");
            //Console.WriteLine(@"Data Source=project.db");
            
            //optionsBuilder.UseSqlite(sqlitePath);
            optionsBuilder.UseSqlite("Data Source=" + sqlitePath + "\\project.db");
            //@"Server=(localdb)\mssqllocaldb;Database=Blogging;Integrated Security=True");
        }
    }
}

// export PATH="$PATH:/home/student/.dotnet/tools"