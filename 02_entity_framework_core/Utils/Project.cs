using System;

namespace Utils
{
    public class Project
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime CreationDate { get; set; }
        
        public Uri? Uri { get; set; }
        
        public string? musiBycNowePole { get; set; }
    }
}

//3. dotnet ef migraition ...
//4. dotnet ef database update