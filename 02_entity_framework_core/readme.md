# Entity Framework Core

In order to use EF first we have to add appropriate package.
Package will be added only to **Utils** and will propagate to packages using this library.

```bash
cd Utils/
dotnet add package Microsoft.EntityFrameworkCore --version 3.0.0-preview8.19405.11
```

Second step is to create class deriving from **DbContext** that contains our data.
File is also added in **Utils** library.

```bash
cat Utils/Context.cs
```

In order to use actual DB we have to install providers.
Good information is that providers are needed only in packages that use our context.
The **Utils** library itself does not have to reference any concrete provider.

```bash
cd App/
dotnet add package Microsoft.EntityFrameworkCore.Sqlite --version 3.0.0-preview8.19405.11
```

And for tests simple in-memory version:

```bash
cd Test/
dotnet add package Microsoft.EntityFrameworkCore.InMemory --version 3.0.0-preview8.19405.11
```

After this three steps we should be able to write some tests and try executing application.
At this point application execution will fail with with error saying that there is no **Projects** table.

To initialize database schema in **App** project we first need to install EF command line tool if not installed.
To check whether tool is present execute one of following commands:

```bash
dotnet-ef
dotnet ef
```

If command/tool is missing execute:

```bash
dotnet tool install --global dotnet-ef --version 3.0.0-preview8.19405.11
```

And according to suggestion from install command output:

```bash
cat << \EOF >> ~/.bashrc
# Add .NET Core SDK tools
export PATH="$PATH:/home/student/.dotnet/tools"
EOF
```

For this tool to work properly we also have to reference designer package:

```bash
cd App/
dotnet add package Microsoft.EntityFrameworkCore.Design --version 3.0.0-preview8.19405.11
```

Now we can generate our first migration.

```bash
cd App/
dotnet ef migrations add Initial
```

Now comes the question - how to deal with inspections/coverage in generated code?
Quick answer - disable them!

```c#
// ReSharper disable All
using System.Diagnostics.CodeAnalysis;

[ExcludeFromCodeCoverage]
class UglyGeneratedClass
{

}
```

Final step before we can run application is to update the DB

```bash
dotnet ef database update
```

Similar set of operation has to be performed after we change something in mapped types.

```bash
cd App/
dotnet ef migrations add AddUriToProject
dotnet ef database update
```